//
//  Sign.swift
//  RPC
//
//  Created by Andrew Zaiets on 12.10.16.
//  Copyright © 2016 Andrew Zaiets. All rights reserved.
//

import Foundation
import GameplayKit

let randomChoice = GKRandomDistribution(lowestValue: 0, highestValue: 2)

enum Sign{
    case scissors
    case rock
    case paper
    
    func randomSign() -> Sign {
        let sign = randomChoice.nextInt()
        if sign == 0 {
            return .rock
        } else if sign == 1 {
            return .paper
        } else {
            return .scissors
        }
    }
    
    
    func compare(opponentSign: Sign) -> GameState {
        switch opponentSign {
        case .scissors:
            switch self {
            case .scissors:
                return GameState.draw
            case .rock:
                return GameState.lose
            case .paper:
                return .win
            default:
                return GameState.start
            }
        case .rock:
            switch self {
            case .scissors:
                return GameState.win
            case .rock:
                return GameState.draw
            case .paper:
                return .lose
            default:
                return GameState.start
            }
        case .paper:
            switch self {
            case .scissors:
                return GameState.lose
            case .rock:
                return GameState.win
            case .paper:
                return .draw
            default:
                return GameState.start
            }

        default:
            return GameState.start
        }
    }
    
}
