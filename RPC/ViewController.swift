//
//  ViewController.swift
//  RPC
//
//  Created by Andrew Zaiets on 12.10.16.
//  Copyright © 2016 Andrew Zaiets. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var stateLabel: UILabel!
    
    @IBOutlet weak var signView: UIStackView!
    
    @IBOutlet weak var robotEmoji: UILabel!
    
    
    private var _robotChoice:Sign = Sign.paper
    
    private var _playerChoice:Sign = Sign.paper
    
    private var _gameState:GameState = GameState.start
    
    @IBOutlet weak var againButton: UIButton!
    
    
    @IBAction func paperChoice(_ sender: UIButton) {
       _robotChoice = _robotChoice.randomSign()
       _playerChoice = Sign.paper
       _gameState = _playerChoice.compare(opponentSign: _robotChoice)
        
        currentGameState(gameState: _gameState)
        
        againButton.isHidden = false

    }
    
    @IBAction func scissorChoice(_ sender: UIButton) {
        _robotChoice = _robotChoice.randomSign()
        _playerChoice = Sign.paper
        _gameState = _playerChoice.compare(opponentSign: _robotChoice)
        
        currentGameState(gameState: _gameState)
        
        againButton.isHidden = false
        againButton.isEnabled = true

    }
    
    @IBAction func rockChoice(_ sender: UIButton) {
        _robotChoice = _robotChoice.randomSign()
        _playerChoice = Sign.paper
        _gameState = _playerChoice.compare(opponentSign: _robotChoice)
        
        currentGameState(gameState: _gameState)
        
        againButton.isHidden = false

    }
    
    @IBAction func repeatAct(_ sender: UIButton) {
        loadView()
    }
    
    
    private func currentGameState(gameState:GameState){
        switch gameState {
        case .draw:
            stateLabel.text = "Draw!"
            robotEmoji.text = "😐"
            mainView.backgroundColor = .yellow
            signView.isHidden = true
        case .win:
            stateLabel.text = "Win!"
            robotEmoji.text = "😒"
            mainView.backgroundColor = .green
            signView.isHidden = true
        case .lose:
            stateLabel.text = "Lose"
            robotEmoji.text = "😏"
            mainView.backgroundColor = .red
            signView.isHidden = true
        default:
            break
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        againButton.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

