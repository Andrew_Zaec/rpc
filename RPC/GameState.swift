//
//  GameState.swift
//  RPC
//
//  Created by Andrew Zaiets on 12.10.16.
//  Copyright © 2016 Andrew Zaiets. All rights reserved.
//

import Foundation

enum GameState{
    case start,win,lose,draw
}
